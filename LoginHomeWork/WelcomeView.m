//
//  WelcomeView.m
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "WelcomeView.h"

@implementation WelcomeView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor whiteColor];
        
        self.welcomeLabel = [[UILabel alloc] init];
        [self addSubview:self.welcomeLabel];
        
        self.imagePickerButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [self.imagePickerButton setTitle:@"Pick Image" forState:UIControlStateNormal];
        [self addSubview:self.imagePickerButton];
        
        self.userImage = [[UIImageView alloc] init];
        self.userImage.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
        [self addSubview:self.userImage];
        
        [self makeConstraints];
    }
    return self;
}

-(void) makeConstraints
{
    self.welcomeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.welcomeLabel.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.welcomeLabel.topAnchor constraintGreaterThanOrEqualToAnchor:self.topAnchor constant:100].active = YES;
    [self.welcomeLabel.widthAnchor constraintEqualToConstant:200.0].active = YES;
    [self.welcomeLabel.heightAnchor constraintEqualToConstant:20.0].active = YES;
    
    self.imagePickerButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.imagePickerButton.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.imagePickerButton.topAnchor constraintGreaterThanOrEqualToAnchor:self.welcomeLabel.bottomAnchor constant:10.0].active = YES;
    [self.imagePickerButton.widthAnchor constraintEqualToConstant:200.0].active = YES;
    [self.imagePickerButton.heightAnchor constraintEqualToConstant:20.0].active = YES;
    
    self.userImage.translatesAutoresizingMaskIntoConstraints = NO;
    [self.self.userImage.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.userImage.topAnchor constraintGreaterThanOrEqualToAnchor:self.imagePickerButton.topAnchor constant:50].active = YES;
    [self.userImage.widthAnchor constraintEqualToConstant:200.0].active = YES;
    [self.userImage.heightAnchor constraintEqualToConstant:200.0].active = YES;
}


@end
