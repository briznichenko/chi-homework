//
//  WelcomeViewControllerModel.m
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "WelcomeViewControllerModel.h"

@implementation WelcomeViewControllerModel

-(instancetype)initWithData:(NSString*) firstName lastName:(NSString*) lastName
{
    self = [super init];
    if(self)
    {
        self.firstName = firstName;
        self.lastName = lastName;
    }
    return self;
}

@end
