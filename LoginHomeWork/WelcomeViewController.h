//
//  WelcomeViewController.h
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "WelcomeView.h"
#import "WelcomeViewControllerModel.h"

@interface WelcomeViewController : UIViewController
<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) WelcomeViewControllerModel *welcomeControllerModel;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (strong, nonatomic) WelcomeView *welcomeView;

-(instancetype)initWithModel:(NSString*) firstName lastName:(NSString*) lastName;

@end
