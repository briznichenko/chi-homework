//
//  WelcomeView.h
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeView : UIView

@property(strong, nonatomic) UILabel *welcomeLabel;
@property(strong, nonatomic) UIImageView *userImage;
@property(strong, nonatomic) UIButton *imagePickerButton;

@end
