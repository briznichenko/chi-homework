//
//  LoginView.m
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "LoginView.h"

@implementation LoginView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.usernameTextField = [[UITextField alloc] init];
        self.usernameTextField.placeholder = @"First name here";
        self.usernameTextField.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.09];
        [self addSubview:self.usernameTextField];
        
        self.lastNameTextField = [[UITextField alloc] init];
        self.lastNameTextField.placeholder = @"Last name here";
        self.lastNameTextField.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.09];
        [self addSubview:self.lastNameTextField];
        
        self.showUserInfoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.showUserInfoButton setTitle:@"Login" forState:UIControlStateNormal];
        self.showUserInfoButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        [self.showUserInfoButton setTintColor:[UIColor whiteColor]];
        [self addSubview:self.showUserInfoButton];
        
        [self makeConstraints];
    }
    return self;

}

-(void) makeConstraints
{
    self.usernameTextField.translatesAutoresizingMaskIntoConstraints = NO;
    [self.usernameTextField.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.usernameTextField.topAnchor constraintGreaterThanOrEqualToAnchor:self.topAnchor constant:100].active = YES;
    [self.usernameTextField.widthAnchor constraintEqualToConstant:200.0].active = YES;
    [self.usernameTextField.heightAnchor constraintEqualToConstant:20.0].active = YES;
    
    self.lastNameTextField.translatesAutoresizingMaskIntoConstraints = NO;
    [self.lastNameTextField.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.lastNameTextField.topAnchor constraintGreaterThanOrEqualToAnchor:self.usernameTextField.bottomAnchor constant:10.0].active = YES;
    [self.lastNameTextField.widthAnchor constraintEqualToConstant:200.0].active = YES;
    [self.lastNameTextField.heightAnchor constraintEqualToConstant:20.0].active = YES;
    
    self.showUserInfoButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.showUserInfoButton.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.showUserInfoButton.topAnchor constraintGreaterThanOrEqualToAnchor:self.lastNameTextField.topAnchor constant:50].active = YES;
    [self.showUserInfoButton.widthAnchor constraintEqualToConstant:50.0].active = YES;
    [self.showUserInfoButton.heightAnchor constraintEqualToConstant:50.0].active = YES;
}

@end
