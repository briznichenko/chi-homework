//
//  LoginViewContoller.h
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewModel.h"
#import "LoginView.h"

@interface LoginViewController : UIViewController

@property(strong, nonatomic) LoginViewModel *loginModel;
@property(strong, nonatomic) LoginView *loginView;

@end
