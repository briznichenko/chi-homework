//
//  WelcomeViewControllerModel.h
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WelcomeViewControllerModel : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;

-(instancetype)initWithData:(NSString*) firstName lastName:(NSString*) lastName;


@end
