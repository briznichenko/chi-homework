//
//  WelcomeViewController.m
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

#pragma mark -- init

-(instancetype)initWithModel:(NSString*) firstName lastName:(NSString*) lastName
{
    self = [super init];
    if(self)
    {
        self.welcomeControllerModel = [[WelcomeViewControllerModel alloc] initWithData:firstName lastName:lastName];
    }
    return self;
}



#pragma mark -- view

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.welcomeView = [[WelcomeView alloc] initWithFrame:self.navigationController.viewControllers[0].view.frame];
    self.welcomeView.welcomeLabel.text = [NSString stringWithFormat:@"Welcome, %@ %@",
                                          self.welcomeControllerModel.firstName,
                                          self.welcomeControllerModel.lastName];
    [self.view addSubview:self.welcomeView];
    
    [self.welcomeView.imagePickerButton addTarget:self action:@selector(imagePickerButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)imagePickerButtonPressed
{
    self.imagePicker = [[UIImagePickerController alloc]init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        self.imagePicker.delegate = self;
    }
    
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status)
        {
            [self presentViewController:self.imagePicker animated:true completion:nil];
        }];
    } else if (status == PHAuthorizationStatusAuthorized) {
        [self presentViewController:self.imagePicker animated:true completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *userImage = info[UIImagePickerControllerOriginalImage];
    [self.imagePicker dismissViewControllerAnimated:YES completion:nil];
    self.welcomeView.userImage.image = userImage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
