//
//  LoginViewContoller.m
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "LoginViewController.h"
#import "WelcomeViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect viewFrame = CGRectMake(self.view.frame.origin.x,
                                  self.view.frame.origin.y + self.navigationController.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height);
    self.loginView = [[LoginView alloc] initWithFrame:viewFrame];
    [self.view addSubview:self.loginView];
    
    self.loginModel = [[LoginViewModel alloc] init];
    
    [self.loginView.showUserInfoButton addTarget:self action:@selector(loginButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(void)loginButtonPressed
{
    self.loginModel.firstName = self.loginView.usernameTextField.text;
    self.loginModel.lastName = self.loginView.lastNameTextField.text;
    WelcomeViewController *welcomeView = [[WelcomeViewController alloc] initWithModel:self.loginModel.firstName lastName: self.loginModel.lastName];
    [self.navigationController pushViewController:welcomeView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
