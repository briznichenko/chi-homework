//
//  LoginView.h
//  LoginHomeWork
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginView : UIView

@property(strong, nonatomic) UITextField *usernameTextField;
@property(strong, nonatomic) UITextField *lastNameTextField;
@property(strong, nonatomic) UIButton *showUserInfoButton;

@end
